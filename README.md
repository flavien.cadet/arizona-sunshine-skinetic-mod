# Arizona Sunshine - Skinetic Mod

<p align="center">

[![Skinetic Discord](https://dcbadge.vercel.app/api/server/55u4DsCP82)](https://discord.gg/55u4DsCP82)

</p>

**Support for Skinetic haptic vest on Arizona Sunshine.**

> ⚠ This mod uses *Skinetic SDK v1.2.0* and must be used with Skinetic Vest version 1.1.3 or later


## Description

This mod adds haptic feedback to the Skinetic vest.

It provides distinct effects for these in-game interactions:
- Gun Recoil _(left/right/empty)_
- Shotgun Recoil _(left/right/empty)_
- Sniper Recoil _(left/right/empty)_
- Rifle Recoil _(left/right/two handed/empty)_
- Grenade Launcher Recoil _(left/right/empty)_
- Weapon reload
- Store item to belt
- Store ammo to belt
- Low health level
- Hit by a zombie
- Healing
- Death


---

## Table of Contents

- [Usage](#usage)
    - [Requirements](#requirements)
    - [Install](#install)
    - [Launch](#launch)
    - [Settings](#settings)
    - [Edit Haptic Patterns](#edit-haptic-patterns)
- [Mod Edition](#mod-edition)
- [Version History](#version-history)
- [License](#license)
- [Help](#help)

----

## Usage

### Requirements

- [**Skinetic**](https://www.skinetic.actronika.com/) Vest
- [**Arizona Sunshine**](https://vertigo-games.com/games/arizona-sunshine) for PCVR
- [**MelonLoader**](https://melonwiki.xyz)

### Install

- Install MelonLoader into your game folder (see [MelonLoader quickstart](https://melonwiki.xyz/#/README))
- Extract the mod archive to the root folder of your game
    - If you use Steam with default game location, it should be in `C:\Program Files\Steam\steamapps\common\ArizonaSunshine`
    - It should put `SkineticSDK.dll` in the _MelonLoader_ folder, and `ArizonaSunshine_SkineticMod.dll` in _Mods_

### Launch

- Turn on the Skinetic vest
- Connect it with usb or turn on Bluetooth and pair it to your computer
- Start the game as usual

> The vest will indicate its connection with a small haptic feedback.

### Settings

On first start, the mod should create a `SkineticSettings.cfg` setting file inside the _UserData_ directory.

The `SkineticSettings.cfg` file can be edited with any text editor. It contains 2 editable values:
- **DefaultVolume**: Controls the global volume of the patterns (in %).
     The value can be set from 0 to 250. At 100, the patterns base volume is preserved and rendered as intended. Between 0 and 99, it's attenuated. Between 101 and 250, the volume is increased, which can lead to distortion of the effects and product audible noise.

- **SerialNumber**: Default value is 0. This is the serial number of the Skinetic vest the mod will try to connect to. 0 means the first available Skinetic vest. *This setting should only be changed if multiple vests are paired to the PC being used.*

- **BoostPercent**: int. From 0 to 100. Default value is 0%.
    This setting allows the end user to globally boost the effects played on the vest. At 0%, no boost is applied. From 1 to 100%, each pattern is **altered** to create a more punchy feel on Skinetic. For this mod, keeping this value below 20% will preserve most of the intended feel of each effect.
    
### Edit Haptic Patterns

At first start, the mod should also create a new folder `SkineticPatterns` with the different haptic patterns inside the _UserData_ directory.

Patterns located in `UserData/SkineticPatterns/` can be modified using [**Unitouch Studio**](https://www.skinetic.actronika.com/unitouch-studio).
Each `[patternName].spn` file can be directly loaded and modified in the studio or completely replaced by a new file.
To be triggered correctly, the new file must have the same name as the one it replaces.

To reset the patterns, simply delete or rename the folder containing them. This will force the mod to regenerate them the next times it starts.

---

## Mod edition

For more advanced users and mod developpers, this mod can be easily edited and updated.
See [Mod Edition Documentation](./MODEDITION.md)

## Version History

* 1.0.0
    * Initial release

## License

- This mod is licensed under LGPL-v3.
- It includes Skinetic SDK as a closed-source library. See [Skinetic SDK CGL](./ACTRONIKA%20-%20CGL%20Skinetic%20SDK%20-%20EN.pdf).

## Help

To get help, discuss good practices or report any issue, join the [Skinetic discord server](https://discord.gg/55u4DsCP82).