﻿/*
 * This file is part of Arizona Sunshine Skinetic Mod.
 *
 * Arizona Sunshine Skinetic Mod is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * any later version.
 *
 * Skinetic Mod Template is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with Arizona Sunshine Skinetic Mod.  If not, see <http://www.gnu.org/licenses/>.
 */

using System;
using System.Collections;
using System.Collections.Generic;
using System.Runtime.InteropServices;
using UnityEngine;

namespace Skinetic
{
    public class SkineticWrapping
    {

        /// <summary>
        /// Type of connection to Skinetic.
        /// E_AUTODETECT will try all available type of connection in the following order:
        /// - Bluetooth
        /// - USB
        /// - WIFI
        /// </summary>
        public enum OutputType
        {
            E_AUTODETECT,
            E_BLUETOOTH,
            E_USB,
            E_WIFI,
        }

        /// <summary>
        /// Type of Skinetic device.
        /// </summary>
        public enum DeviceType
        {
            E_UNKNOWN = unchecked((int)0xFFFFFFFF),
            E_VEST = 0x01100101,
        }

        /// <summary>
        /// Connection state.
        /// </summary>
        public enum ConnectionState
        {
            E_RECONNECTING = 3,          /**< Device connection was broken, trying to reconnect */
            E_DISCONNECTING = 2,         /**< Device is disconnecting, releasing all resources */
            E_CONNECTING = 1,            /**< Connection to the device is being established, connection routine is active. */
            E_CONNECTED = 0,             /**< Device is connected*/
            E_DISCONNECTED = -1,         /**< Device is disconnected */
        }


        [System.Serializable, System.Runtime.InteropServices.StructLayout(System.Runtime.InteropServices.LayoutKind.Sequential, Pack = 8)]
        public struct CDeviceInfo
        {
            /** Available Output connection mode.*/
            public SkineticWrapping.OutputType outputType;
            /** Device Serial Number.*/
            public System.UInt32 serialNumber;
            /** Device Type.*/
            public SkineticWrapping.DeviceType deviceType;
            /** Device Version.*/
            [MarshalAs(UnmanagedType.LPStr)] public string deviceVersion;
            /** Pointer to next device.*/
            public IntPtr next;
        }

        /// <summary>
        /// Effect's state.
        /// </summary>
        public enum EffectState
        {
            E_PLAY = 2,
            E_MUTE = 1,
            E_INITIALIZED = 0,
            E_STOP = -1
        }

        public struct DeviceInfo
        {
            /** Available Output connection mode.*/
            public OutputType outputType;
            /** Device Serial Number.*/
            public System.UInt32 serialNumber;
            /** Device Type.*/
            public DeviceType deviceType;
            /** Device Version.*/
            public string deviceVersion;
        }


        /// <summary>
        /// Structure containing all properties of an haptic effect instance.
        /// </summary>
        [System.Serializable, System.Runtime.InteropServices.StructLayout(System.Runtime.InteropServices.LayoutKind.Sequential, Pack = 8)]
        public struct EffectProperties
        {
            /** priority (default - 5): level of priority [1; 10] of the effect. In case too many effects are playing
             *      simultaneously, the effect with lowest priority (10) will be muted.*/
            public int priority;
            /** volume (default - 100): percentage of the base volume between [0; 250]%: [0;100[% the pattern attenuated,
             *      100% the pattern's base volume is preserved, ]100; 250]% the pattern is amplified.
             *      Too much amplification may lead to the clipping of the haptic effects, distorting them
             *      and producing audible noise. */
            public float volume;
            /** speed (default - 1): time scale between [0.01; 100]: [0.01; 1[ the pattern is slowed down, 1 the pattern
             *      timing is preserved, ]1; 100] the pattern is accelerated. The resulting speed between
             *      the haptic effect's and the samples' speed within the pattern cannot exceed these
             *      bounds. Slowing down or accelerating a sample too much may result in an haptically poor
             *      effect.*/
            public float speed;
            /** repeatCount (default - 1): number of repetition of the pattern if the maxDuration is not reached.
             *      If 0, the pattern is repeat indefinitely until it is either stopped with stopEffect()
             *      or reach the maxDuration value.*/
            public int repeatCount;
            /** repeatDelay (default - 0): pause in second between to repetition of the pattern, this value is not
             *      affected by the speed parameter.*/
            public float repeatDelay;
            /** playAtTime (default - 0): time in the pattern at which the effect start to play. This value need to be
             *      lower than the maxDuration. It also takes into account the repeatCount and the
             *      repeatDelay of the pattern.*/
            public float playAtTime;
            /** maxDuration (default - 0): maximum duration of the effect, it is automatically stopped if the duration
             *      is reached without any regards for the actual state of the repeatCount. A maxDuration
             *      of 0 remove the duration limit, making the effect ables to play indefinitely.*/
            public float maxDuration;
            /** height (default - 0): height (in meter) to translate the pattern by.*/
            public float height;
            /** heading (default - 0): heading angle (in degree) to rotate the pattern by in the horizontal plan.*/
            public float heading;
            /** tilting (default - 0): tilting angle (in degree) to rotate the pattern by in the sagittal plan.*/
            public float tilting;

            public EffectProperties(int priority, float volume, float speed, int repeatCount, float repeatDelay,
                                    float playAtTime, float maxDuration, float height, float heading, float tilting)
            {
                this.priority = priority;
                this.volume = volume;
                this.speed = speed;
                this.repeatCount = repeatCount;
                this.repeatDelay = repeatDelay;
                this.playAtTime = playAtTime;
                this.maxDuration = maxDuration;
                this.height = height;
                this.heading = heading;
                this.tilting = tilting;
            }
        }

        private const string DLLNAME = @".\MelonLoader\SkineticSDK";

        [global::System.Runtime.InteropServices.DllImport(DLLNAME, EntryPoint = "ski_createSDKInstance")]
        private static extern int Ski_createSDKInstance([MarshalAs(UnmanagedType.LPStr)] string logFileName);

        [global::System.Runtime.InteropServices.DllImport(DLLNAME, EntryPoint = "ski_freeSDKInstance")]
        private static extern void Ski_freeSDKInstance(int sdk_ID);

        [global::System.Runtime.InteropServices.DllImport(DLLNAME, EntryPoint = "ski_scanDevices")]
        private static extern int Ski_scanDevices(int sdk_ID, SkineticWrapping.OutputType outputType);

        [global::System.Runtime.InteropServices.DllImport(DLLNAME, EntryPoint = "ski_scanStatus")]
        private static extern int Ski_scanStatus(int sdk_ID);

        [global::System.Runtime.InteropServices.DllImport(DLLNAME, EntryPoint = "ski_getFirstScannedDevice")]
        private static extern IntPtr Ski_getFirstScannedDevice(int sdk_ID);

        [global::System.Runtime.InteropServices.DllImport(DLLNAME, EntryPoint = "ski_connectDevice")]
        private static extern int Ski_connectDevice(int sdk_ID, SkineticWrapping.OutputType outputType, UInt32 serialNumber);

        [global::System.Runtime.InteropServices.DllImport(DLLNAME, EntryPoint = "ski_disconnectDevice")]
        private static extern int Ski_disconnectDevice(int sdk_ID);

        [global::System.Runtime.InteropServices.DllImport(DLLNAME, EntryPoint = "ski_connectionStatus")]
        private static extern SkineticWrapping.ConnectionState Ski_connectionStatus(int sdk_ID);

        [global::System.Runtime.InteropServices.DllImport(DLLNAME, EntryPoint = "ski_setConnectionCallback")]
        private static extern int Ski_setConnectionCallback(int sdk_ID, IntPtr callback, IntPtr userData);

        [global::System.Runtime.InteropServices.DllImport(DLLNAME, EntryPoint = "ski_getSDKVersion")]
        private static extern IntPtr Ski_getSDKVersion(int sdk_ID);

        [global::System.Runtime.InteropServices.DllImport(DLLNAME, EntryPoint = "ski_getSkineticSerialNumber")]
        private static extern UInt32 Ski_getSkineticSerialNumber(int sdk_ID);

        [global::System.Runtime.InteropServices.DllImport(DLLNAME, EntryPoint = "ski_getSkineticVersion")]
        private static extern IntPtr Ski_getSkineticVersion(int sdk_ID);

        [global::System.Runtime.InteropServices.DllImport(DLLNAME, EntryPoint = "ski_getSkineticType")]
        private static extern SkineticWrapping.DeviceType Ski_getSkineticType(int sdk_ID);

        [global::System.Runtime.InteropServices.DllImport(DLLNAME, EntryPoint = "ski_getIntensityBoostPercent")]
        private static extern int Ski_getIntensityBoostPercent(int sdk_ID);

        [global::System.Runtime.InteropServices.DllImport(DLLNAME, EntryPoint = "ski_setIntensityBoostPercent")]
        private static extern int Ski_setIntensityBoostPercent(int sdk_ID, int boostPercent);

        [global::System.Runtime.InteropServices.DllImport(DLLNAME, EntryPoint = "ski_loadPatternFromJSON")]
        private static extern int Ski_loadPatternFromJSON(int sdk_ID, [MarshalAs(UnmanagedType.LPStr)] string json);

        [global::System.Runtime.InteropServices.DllImport(DLLNAME, EntryPoint = "ski_unloadPattern")]
        private static extern int Ski_unloadPattern(int sdk_ID, int patternID);

        [global::System.Runtime.InteropServices.DllImport(DLLNAME, EntryPoint = "ski_playEffect")]
        private static extern int Ski_playEffect(int sdk_ID, int patternID, EffectProperties effectProperties);

        [global::System.Runtime.InteropServices.DllImport(DLLNAME, EntryPoint = "ski_stopEffect")]
        private static extern int Ski_stopEffect(int sdk_ID, int effectID, float time);

        [global::System.Runtime.InteropServices.DllImport(DLLNAME, EntryPoint = "ski_effectState")]
        private static extern SkineticWrapping.EffectState Ski_effectState(int sdk_ID, int effectI);

        [global::System.Runtime.InteropServices.DllImport(DLLNAME, EntryPoint = "ski_pauseAll")]
        private static extern int Ski_pauseAll(int sdk_ID);

        [global::System.Runtime.InteropServices.DllImport(DLLNAME, EntryPoint = "ski_resumeAll")]
        private static extern int Ski_resumeAll(int sdk_ID);

        [global::System.Runtime.InteropServices.DllImport(DLLNAME, EntryPoint = "ski_stopAll")]
        private static extern int Ski_stopAll(int sdk_ID);

        private int m_instanceID = -1;
        private GCHandle m_handle;

        /// <summary>
        /// Initialize the Skinetic device instance.
        /// </summary>
        public void InitInstance()
        {
            if (m_instanceID != -1)
                return;
            m_instanceID = Ski_createSDKInstance("");
            m_handle = GCHandle.Alloc(this, GCHandleType.Normal);
        }

        /// <summary>
        /// Deinitialize the Skinetic device instance.
        /// </summary>
        public void DeinitInstance()
        {
            if (m_instanceID == -1)
                return;
            Ski_freeSDKInstance(m_instanceID);
            m_instanceID = -1;
            m_handle.Free();
        }

        /// <summary>
        /// Initialize a scanning routine to find all available Skinetic device.
        /// the state of the routine can be obtain from ScanStatus(). Once completed,
        /// the result can be accessed using GetScannedDevices().
        /// </summary>
        /// <param name="output"></param>
        /// <returns>0 on success, an Error code on failure.</returns>
        public int ScanDevices(SkineticWrapping.OutputType output)
        {
            return Ski_scanDevices(m_instanceID, output);
        }

        /// <summary>
        /// Check the status of the asynchronous scanning routine.
        /// The method returns:
        ///  - 1 if the scan is ongoing.
        ///  - 0 if the scan is completed.
        ///  - a negative error code if the connection failed.
        /// The asynchronous scan routine is terminated on failure.
        /// Once the scan is completed, the result can be obtain by calling GetScannedDevices().
        /// </summary>
        /// <returns>the current status or an error on failure.</returns>
        public int ScanStatus()
        {
            return Ski_scanStatus(m_instanceID);
        }

        /// <summary>
        /// This function returns a list of all DeviceInfo of each Skinetic devices found during the scan
        /// which match the specified output type.
        /// </summary>
        /// <returns>a list of DeviceInfo, empty if no match or an error occurs.</returns>
        public List<SkineticWrapping.DeviceInfo> GetScannedDevices()
        {
            List<SkineticWrapping.DeviceInfo> listDevices = new List<SkineticWrapping.DeviceInfo>();
            IntPtr res = Ski_getFirstScannedDevice(m_instanceID);
            if (res == IntPtr.Zero)
            {
                return listDevices;
            }
            CDeviceInfo cdevice = Marshal.PtrToStructure<CDeviceInfo>(Ski_getFirstScannedDevice(m_instanceID));
            Skinetic.SkineticWrapping.DeviceInfo device = new SkineticWrapping.DeviceInfo();

            device.deviceType = cdevice.deviceType;
            device.deviceVersion = cdevice.deviceVersion;
            device.serialNumber = cdevice.serialNumber;
            device.outputType = cdevice.outputType;
            listDevices.Add(device);
            while (cdevice.next != IntPtr.Zero)
            {
                cdevice = Marshal.PtrToStructure<CDeviceInfo>(cdevice.next);
                device = new SkineticWrapping.DeviceInfo();

                device.deviceType = cdevice.deviceType;
                device.deviceVersion = cdevice.deviceVersion;
                device.serialNumber = cdevice.serialNumber;
                device.outputType = cdevice.outputType;
                listDevices.Add(device);
            }
            return listDevices;
        }

        /// <summary>
        /// Initialize an asynchronous connection to a Skinetic device using the selected type of connection.
        /// The state of the routine can be obtain from ConnectionStatus().
        /// If the serial number is set to 0, the connection will be performed on the first found device.
        /// </summary>
        /// <param name="output">output type</param>
        /// <param name="serialNumber">serial number of the Skinetic device to connect to</param>
        /// <returns>0 on success, an error otherwise.</returns>
        public int Connect(SkineticWrapping.OutputType output, System.UInt32 serialNumber)
        {
            return Ski_connectDevice(m_instanceID, output, serialNumber);
        }

        /// <summary>
        /// Disconnect the current Skinetic device.
        /// The disconnection is effective once all resources are released.
        /// The state of the routine can be obtain from ConnectionStatus().
        /// </summary>
        /// <returns>0 on success, an error otherwise.</returns>
        public int Disconnect()
        {
            return Ski_disconnectDevice(m_instanceID);
        }

        /// <summary>
        /// Check the current status of the connection.
        /// The asynchronous connection routine is terminated on failure.
        /// </summary>
        /// <returns>the current status of the connection.</returns>
        public SkineticWrapping.ConnectionState ConnectionStatus()
        {
            return Ski_connectionStatus(m_instanceID);
        }

        /// <summary>
        /// Get SDK version as a string. The format of the string is:
        /// <pre>major.minor.revision</pre>
        /// </summary>
        /// <returns>The version string.</returns>
        public String GetSDKVersion()
        {
            IntPtr ptr = Ski_getSDKVersion(m_instanceID);
            // assume returned string is utf-8 encoded
            return Marshal.PtrToStringAnsi(ptr);
        }

        /// <summary>
        /// Get the connected device's version as a string. The format of the string is:
        /// <pre>major.minor.revision</pre>
        /// </summary>
        /// <returns>The version string if a Skinetic device is connected, an error message otherwise.</returns>
        public String GetDeviceVersion()
        {
            IntPtr ptr = Ski_getSkineticVersion(m_instanceID);
            // assume returned string is utf-8 encoded
            return Marshal.PtrToStringAnsi(ptr);
        }

        /// <summary>
        /// Get the connected device's serial number.
        /// </summary>
        /// <returns>The serial number of the connected Skinetic device if any, 0xFFFFFFFF otherwise.</returns>
        public System.UInt32 GetDeviceSerialNumber()
        {
            return Ski_getSkineticSerialNumber(m_instanceID);
        }

        /// <summary>
        /// Get the connected device's type.
        /// </summary>
        /// <returns>The type of the connected Skinetic device if it is connected,
        /// an ERROR message otherwise.</returns>
        public SkineticWrapping.DeviceType GetDeviceType()
        {
            return Ski_getSkineticType(m_instanceID);
        }

        /// <summary>
        /// Get the amount of effect's intensity boost.
        /// The boost increases the overall intensity of all haptic effects.
        /// However, the higher the boost activation is, the more the haptic effects are degraded.
        /// </summary>
        /// <returns>the percentage of effect's intensity boost, an ERROR otherwise.</returns>
        public int GetIntensityBoostPercent()
        {
            return Ski_getIntensityBoostPercent(m_instanceID);
        }

        /// <summary>
        /// Set the amount of effect's intensity boost.
        /// The boost increases the overall intensity of all haptic effects.
        /// However, the higher the boost activation is, the more the haptic effects are degraded.
        /// </summary>
        /// <param name="boostPercent">boostPercent percentage of the boost.</param>
        /// <returns>0 on success, an ERROR otherwise.</returns>
        public int SetIntensityBoostPercent(int boostPercent)
        {
            return Ski_setIntensityBoostPercent(m_instanceID, boostPercent);
        }

        /// <summary>
        /// Load a pattern from a valid json into a local haptic asset and return
        /// the corresponding patternID.The patternID is a positive index.
        /// </summary>
        /// <param name="json">describing the pattern</param>
        /// <returns>Positive patternID on success, an error otherwise.</returns>
        public int LoadPatternFromJSON(String json)
        {
            return Ski_loadPatternFromJSON(m_instanceID, json);
        }

        /// <summary>
        /// Unload the pattern from of the corresponding patternID.
        /// </summary>
        /// <param name="patternID">the patternID of the pattern to unload.</param>
        /// <returns>0 on success, an error otherwise.</returns>
        public int UnloadPattern(int patternID)
        {
            return Ski_unloadPattern(m_instanceID, patternID);
        }

        /// <summary>
        /// Play an haptic effect based on a loaded pattern and return the effectID of this instance.
        /// The instance index is positive.Each call to playEffect() using the same patternID
        /// generates a new haptic effect instance totally uncorrelated to the previous ones.
        /// The instance is destroyed once it stops playing.
        /// 
        /// The haptic effect instance reproduces the pattern with variations describes by the following
        /// properties:
        /// - priority: level of priority[1; 10] of the effect.In case too many effects are playing
        ///      simultaneously, the effect with lowest priority(10) will be muted.
        /// - volume: percentage of the base volume between[0; 250]%: [0;100[% the pattern attenuated,
        ///      100% the pattern's base volume is preserved, ]100; 250]% the pattern is amplified.
        ///      Too much amplification may lead to the clipping of the haptic effects, distorting them
        ///      and producing audible noise.
        /// - speed: time scale between [0.01; 100]: [0.01; 1[the pattern is slowed down, 1 the pattern
        ///      timing is preserved,]1; 100] the pattern is accelerated.The resulting speed between
        ///      the haptic effect's and the samples' speed within the pattern cannot exceed these
        ///      bounds.Slowing down or accelerating a sample too much may result in an haptically poor
        ///      effect.
        /// - repeatCount: number of repetition of the pattern if the maxDuration is not reached.
        ///      If 0, the pattern is repeat indefinitely until it is either stopped with stopEffect()
        ///      or reach the maxDuration value.
        /// - repeatDelay: pause in second between to repetition of the pattern, this value is not
        ///      affected by the speed parameter.
        /// - playAtTime: time in the pattern at which the effect start to play.This value need to be
        ///      lower than the maxDuration.It also takes into account the repeatCount and the
        ///      repeatDelay of the pattern.
        ///      - maxDuration: maximum duration of the effect, it is automatically stopped if the duration
        ///      is reached without any regards for the actual state of the repeatCount. A maxDuration
        ///      of 0 remove the duration limit, making the effect ables to play indefinitely.
        ///      This value is not affected by the speed and the playAtTime parameters. 
        ///      
        /// The three next properties allows to apply the pattern on the haptic device at a different location
        /// by translating/rotating it:
        /// - height: normalized height [-1; 1] to translate the pattern by.A positive value translate
        ///      the pattern upwards.
        /// - heading: heading angle (in degree) to rotate the pattern by in the horizontal plan.A positive
        ///      value rotates the pattern to the left of the vest.
        /// - tilting: tilting angle(in degree) to rotate the pattern by in the sagittal plan.A positive
        ///      value rotates the pattern upwards from front to back.
        ///      
        /// The three transformations are applied in this order: tilting, vertical rotation, vertical translation.
        /// The default position of a pattern is the one obtained when these three parameters are set to zero.
        /// The actual use of these 3 parameters depends on the default position of the pattern and the targeted interaction:
        /// e.g.; for a piercing shot, a heading between[-180; 180]° can be combines with a tilting between[-90; 90] when
        /// using a shape-based pattern centered in the middle of the torso; for a environmental effect,
        /// a heading between[-90; 90]° (or[-180; 180]°) can be combines with a tilting between
        /// [-180; 180]° (resp. [-180; 0]°) when using a pattern with shapes centered on the top, etc.
        /// There are no actual bounds to the angles as not to restrict the usage.
        /// Notice that actuator-based patterns cannot be transformed in this version.
        /// Since all effects cannot be rendered simultaneously, the least priority ones are muted until
        /// the more priority ones are stopped of finished rendering. Muted effects are still running,
        /// but not rendered.
        /// 
        /// The priority order is obtain using the priority level: priority increase from 10 to 1. In
        /// of equality, the number of required simultaneous samples is used to determine which effect has the highest
        /// priority: effects using less simultaneous samples have a higher priority.Again, if the number of required
        /// simultaneous samples is the same, the most recent effect has a higher priority.
        /// 
        /// If the pattern is unloaded, the haptic effect is not interrupted.
        /// </summary>
        /// 
        /// <param name="patternID">pattern used by the effect instance.</param>
        /// <param name="volume">amplification [0%; 250]%.</param>
        /// <param name="speed">time scale between [0.01; 100].</param>
        /// <param name="repeatCount">number of repetition of the pattern.</param>
        /// <param name="repeatDelay">pause in second between to repetition.</param>
        /// <param name="playAtTime">time in second to start the pattern at.</param>
        /// <param name="maxDuration">maximum duration of the effect, 0 disable the limit.</param>
        /// <param name="priority">level of priority [1; 10] of the effect.</param>
        /// <param name="height">normalized height to translate the pattern by. A positive value translate the pattern upwards.</param>
        /// <param name="heading">heading angle(in degree) to rotate the pattern by in the horizontal plan.</param>
        /// <param name="tilting">tilting angle(in degree) to rotate the pattern by in the sagittal plan.</param>
        /// <returns>Positive effectID on success, an error otherwise.</returns>
        public int PlayEffect(int patternID, EffectProperties effectProperties)
        {
            return Ski_playEffect(m_instanceID, patternID, effectProperties);
        }

        /// <summary>
        /// Stop the effect instance identified by its effectID.
        /// The effect is stop in "time" seconds with a fade out to prevent abrupt transition. If time
        /// is set to 0, no fadeout are applied and the effect is stopped as soon as possible.
        /// Once an effect is stopped, it is instance is destroyed and its effectID invalidated.
        /// </summary>
        /// <param name="effectID">index identifying the effect.</param>
        /// <param name="time">duration of the fadeout.</param>
        /// <returns>0 on success, an error otherwise.</returns>
        public int StopEffect(int effectID, float time)
        {
            return Ski_stopEffect(m_instanceID, effectID, time);
        }

        /// <summary>
        ///  Get the current state of an effect. If the effectID is invalid, the 'stop' state
        ///  will be return.
        ///
        /// @param effectID index identifying the effect.
        /// @return the current state of the effect.
        /// </summary>
        public SkineticWrapping.EffectState GetEffectState(int effectID)
        {
            return Ski_effectState(m_instanceID, effectID);
        }

        /// <summary>
        /// Pause all haptic effect that are currently playing.
        /// </summary>
        /// <returns>0 on success, an error otherwise.</returns>
        public int PauseAll()
        {
            return Ski_pauseAll(m_instanceID);
        }

        /// <summary>
        /// Resume the paused haptic effects.
        /// </summary>
        /// <returns>0 on success, an error otherwise.</returns>
        public int ResumeAll()
        {
            return Ski_resumeAll(m_instanceID);
        }

        /// <summary>
        /// Stop all playing haptic effect.
        /// </summary>
        /// <returns>0 on success, an error otherwise.</returns>
        public int StopAll()
        {
            return Ski_resumeAll(m_instanceID);
        }
    }
}
