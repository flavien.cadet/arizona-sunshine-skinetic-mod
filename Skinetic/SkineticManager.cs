﻿/*
 * This file is part of Arizona Sunshine Skinetic Mod.
 *
 * Arizona Sunshine Skinetic Mod is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * any later version.
 *
 * Skinetic Mod Template is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with Arizona Sunshine Skinetic Mod.  If not, see <http://www.gnu.org/licenses/>.
 */

using System.IO;
using System;
using System.Resources;
using System.Globalization;
using System.Collections.Generic;
using ArizonaSunshine_SkineticMod;
using System.Collections;
using System.Reflection;
using MelonLoader;

namespace Skinetic
{
    [Flags] // FlagsAttribute enum allows us to combine multiple values of Pattern and ensurw the result is not already used.
    public enum Patterns : Int32 // Int32 type can be changed for Int64 if the limit of 32 patterns is reached
    {
        None = 0, // default - not used
        GunRecoil = 1 << 0,
        RifleRecoil = 1 << 1,
        SniperRecoil = 1 << 2,
        ShotGunRecoil = 1 << 3,
        ZombieHit = 1 << 4,
        Eating = 1 << 5,
        Dead = 1 << 6,
 
        StoreAmmo = 1 << 8,
        RetrieveAmmo = 1 << 9,
        StoreItem = 1 << 10,
        Explosion = 1 << 11,
        LowLife = 1 << 12,

        Left = 1 << 13,
        Right = 1 << 14,
        TwoHanded = 1 << 15,
        Empty = 1 << 16

        // Add new value to link new effect here
        // numeral value should be the next power of two (eg. (1 << 16))
    };

    /// <summary>
    /// Class <c>SkineticManager</c> handles connection/disconnection of the devices, 
    /// automatic patterns loading and basic calls to ease the use from the mod.
    /// </summary>
    class SkineticManager
    {
        private MelonPreferences_Category SkineticSettings;
        private MelonPreferences_Entry<float> GeneralVolume;
        private MelonPreferences_Entry<uint> SerialNumber;
        private MelonPreferences_Entry<int> BoostPercent;

        private SkineticWrapping SkineticInstance;
        private Dictionary<String, int> PatternDict = new Dictionary<string, int>();
        private Dictionary<Patterns, int> PatternsIds = new Dictionary<Patterns, int>();
        private string SkineticPatternsFolder = "SkineticPatterns";

        /// <summary>
        /// <c>SkineticManager</c> constructor inits the Skinetic SDK instance, 
        /// connects to the device according to the parameter entered in the config file 
        /// (0 for the first available, a valid serial number for a specific Skinetic vest),
        /// and loads the pattern available on disk.
        /// </summary>
        public SkineticManager()
        {
            SkineticSettings = MelonPreferences.CreateCategory("SkineticSettings");
            SkineticSettings.SetFilePath("UserData/SkineticSettings.cfg");

            GeneralVolume = SkineticSettings.CreateEntry<float>("GeneralVolume", 100.0f);
            SerialNumber = SkineticSettings.CreateEntry<uint>("SerialNumber", 0);
            BoostPercent = SkineticSettings.CreateEntry<int>("BoostPercent", 0);
            SkineticSettings.SaveToFile();

            SkineticInstance = new SkineticWrapping();
            SkineticInstance.InitInstance();
            SkineticInstance.Connect(SkineticWrapping.OutputType.E_AUTODETECT, SerialNumber.Value);
            if (BoostPercent.Value != 0)
            {
                SkineticInstance.SetIntensityBoostPercent(BoostPercent.Value);
                MelonLogger.Msg("Boost set to " + BoostPercent.Value + "%");

            }
            if (SerialNumber.Value != 0)
                MelonLogger.Msg("Try to connect to Skinetic #" + SerialNumber.Value);
            else
                MelonLogger.Msg("Try to connect to first Skinetic device available");
            LoadPatterns();
        }

        /// <summary>
        /// <c>Disconnect</c> stops all effect playing and handles the device disconnection and sdk deinstantiation.
        /// </summary>
        public void Disconnect()
        {
            SkineticInstance.StopAll();
            SkineticInstance.Disconnect();
            SkineticInstance.DeinitInstance();
            SkineticSettings.SaveToFile();
        }

        /// <summary>
        /// <c>Play</c> method takes a patterns name, check its existence among the loaded patterns
        /// and plays it if existing with the given volume modulated by the general volume set in the config file.
        /// Other parameters can be modulated by using directly the <c>SkineticWrapping::PlayEffect</c> method, such as:
        /// <c>repeatCount</c>, <c>repeatDelay</c>, <c>maxDuration</c> and <c>priority</c>.
        /// </summary>
        /// <param name="Pattern">Id of the pattern to play</param>
        /// <param name="volume">Percentage of the base volume [0%;250%]. At 100% the original volume is preserved.</param>
        /// <param name="speed">Time scale between [0.01; 100]. [0.01; 1[the pattern is slowed down, 1 the pattern
        ///      timing is preserved, ]1; 100] the pattern is accelerated.</param>
        /// <param name="priority">Level of priority [1; 10] of the effect. In case too many effects are playing
        ///      simultaneously, the effect with lowest priority(10) will be muted.</param>
        /// <param name="height">height(in meter) to translate the pattern by.</param>
        /// <param name="heading">heading angle(in degree) to rotate the pattern by in the horizontal plan.</param>
        /// <param name="tilting">tilting angle(in degree) to rotate the pattern by in the sagittal plan.</param>
        /// <returns>Id of the effect played or -1 if the pattern is not found</returns>
        public int Play(Patterns Pattern, float volume = 100.0f, float speed = 1.0f, int priority = 5, float height = 0.0f, float heading = 0.0f, float tilting = 0.0f)
        {
            volume = (volume * GeneralVolume.Value) / 100;
            if (PatternsIds.ContainsKey(Pattern))
            {
                Skinetic.SkineticWrapping.EffectProperties props =  new Skinetic.SkineticWrapping.EffectProperties(priority, volume, speed, 1, 0, 0, 0, height, heading, tilting);
                return SkineticInstance.PlayEffect(PatternsIds[Pattern], props);
            }
            MelonLogger.Msg("Pattern not found: " + Pattern);
            return -1;
        }

        /// <summary>
        /// <c>Stops the effects instance identified by its ID.</c>
        /// </summary>
        /// <param name="EffectID">ID of the effect to stop. (Effect ID is given at play)</param>
        /// <param name="FadeOut">Time in seconds before the effect stops [0s; 3.0s]</param>
        public void Stop(int EffectID, float FadeOut)
        {
            SkineticInstance.StopEffect(EffectID, FadeOut);
        }

        /// <summary>
        /// <c>LoadPatterns</c> method checks if the pattern folder already exists.
        /// If not, it creates it and fills it with the patterns predefined for the mod.
        /// Then, it loads the patterns from the disk into the SDK.
        /// Thus, it allows the end user to change the patterns according to their needs.
        /// </summary>
        private void LoadPatterns()
        {
            string fullPatternsPath = Directory.GetCurrentDirectory() + @"\" + SkineticPatternsFolder;
            if (!Directory.Exists(fullPatternsPath))
            {
                MelonLogger.Msg("Skinetic patterns folder not found. Attempt to create folder and patterns at '" + fullPatternsPath + "'");
                CreatePatternFiles(fullPatternsPath);
            }

            LoadPattern(Patterns.GunRecoil | Patterns.Left, "GunRecoil_Left");
            LoadPattern(Patterns.GunRecoil | Patterns.Right, "GunRecoil_Right");

            LoadPattern(Patterns.GunRecoil | Patterns.Empty | Patterns.Right, "GunEmpty_Right");
            LoadPattern(Patterns.GunRecoil | Patterns.Empty | Patterns.Left, "GunEmpty_Left");
            
            LoadPattern(Patterns.RifleRecoil | Patterns.Left, "RifleRecoil_Left");
            LoadPattern(Patterns.RifleRecoil | Patterns.Right, "RifleRecoil_Right");
            LoadPattern(Patterns.RifleRecoil | Patterns.TwoHanded | Patterns.Left, "RifleRecoil_TwoHanded_Left");
            LoadPattern(Patterns.RifleRecoil | Patterns.TwoHanded | Patterns.Right, "RifleRecoil_TwoHanded_Right");

            LoadPattern(Patterns.RifleRecoil | Patterns.Empty | Patterns.Right, "RifleEmpty_Right");
            LoadPattern(Patterns.RifleRecoil | Patterns.Empty | Patterns.Left, "RifleEmpty_Left");
            LoadPattern(Patterns.RifleRecoil | Patterns.TwoHanded | Patterns.Empty | Patterns.Right, "RifleEmpty_TwoHanded_Right");
            LoadPattern(Patterns.RifleRecoil | Patterns.TwoHanded | Patterns.Empty | Patterns.Left, "RifleEmpty_TwoHanded_Left");

            LoadPattern(Patterns.SniperRecoil | Patterns.Left, "SniperRecoil_Left");
            LoadPattern(Patterns.SniperRecoil | Patterns.Right, "SniperRecoil_Right");

            LoadPattern(Patterns.SniperRecoil | Patterns.Empty | Patterns.Right, "SniperEmpty_Right");
            LoadPattern(Patterns.SniperRecoil | Patterns.Empty | Patterns.Left, "SniperEmpty_Left");

            LoadPattern(Patterns.ShotGunRecoil | Patterns.Left, "ShotgunRecoil_Left");
            LoadPattern(Patterns.ShotGunRecoil | Patterns.Right, "ShotgunRecoil_Right");
            LoadPattern(Patterns.ShotGunRecoil | Patterns.TwoHanded | Patterns.Left, "ShotgunRecoil_TwoHanded_Left");
            LoadPattern(Patterns.ShotGunRecoil | Patterns.TwoHanded | Patterns.Right, "ShotgunRecoil_TwoHanded_Right");

            LoadPattern(Patterns.ShotGunRecoil | Patterns.Empty | Patterns.Right, "ShotgunEmpty_Right");
            LoadPattern(Patterns.ShotGunRecoil | Patterns.Empty | Patterns.Left, "ShotgunEmpty_Left");
            LoadPattern(Patterns.ShotGunRecoil | Patterns.TwoHanded | Patterns.Empty | Patterns.Left, "ShotgunEmpty_TwoHanded_Left");
            LoadPattern(Patterns.ShotGunRecoil | Patterns.TwoHanded | Patterns.Empty | Patterns.Right, "ShotgunEmpty_TwoHanded_Right");


            LoadPattern(Patterns.ZombieHit, "ZombieHit");
            LoadPattern(Patterns.Eating, "Eating");
            LoadPattern(Patterns.Dead, "Dead");
            LoadPattern(Patterns.StoreAmmo, "StoreAmmo");
            LoadPattern(Patterns.RetrieveAmmo, "RetrieveAmmo");
            LoadPattern(Patterns.StoreItem, "StoreItem");
            LoadPattern(Patterns.Explosion, "Explosion");
            LoadPattern(Patterns.LowLife, "LowLife");

            // To load new pattern in SkineticSDK:
            //LoadPattern(Patterns.[NewValue], [spn file name]);
        }

        private void LoadPattern(Patterns patternId, string patternFileName)
        {
            MelonLogger.Msg("--- " + patternId + " | " + patternFileName);
            string fullPath = SkineticPatternsFolder + "\\" + patternFileName + ".spn";
            if (!File.Exists(fullPath))
                return;
            MelonLogger.Msg("Skinetic - load pattern: " + patternId + " | " + patternFileName);
            string spnContent = File.ReadAllText(fullPath);
            PatternsIds.Add(patternId, SkineticInstance.LoadPatternFromJSON(spnContent));
        }

        /// <summary>
        /// <c>CreatePatternFiles</c> method creates a folder at the given path and fill it with the patterns predefined for the mod.
        /// </summary>
        /// <param name="patternPath"></param>
        private void CreatePatternFiles(string patternPath)
        {
            Directory.CreateDirectory(patternPath);

            ResourceSet res = ArizonaSunshine_SkineticMod.Properties.Resources.ResourceManager.GetResourceSet(CultureInfo.InvariantCulture, true, true);
            foreach (DictionaryEntry dict in res)
            {
                var filePath = patternPath + "\\" + dict.Key + ".spn";
                File.WriteAllText(filePath, dict.Value.ToString());
            }
        }
    }
}
